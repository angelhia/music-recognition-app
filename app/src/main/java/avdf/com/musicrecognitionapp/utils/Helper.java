package avdf.com.musicrecognitionapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Angelhia on 27/7/2017.
 */

public class Helper {
    /**
     * Helpers to read license file from assets as string
     */
    public static String getAssetAsString(Context context, String assetName) {
        String assetString = null;
        InputStream assetStream;
        try {
            assetStream = context.getAssets().open(assetName);
            if (assetStream != null) {
                java.util.Scanner s = new java.util.Scanner(assetStream).useDelimiter("\\A");
                assetString = s.hasNext() ? s.next() : "";
                assetStream.close();
            } else {
                Log.e(context.getClass().getSimpleName(), "Asset not found:" + assetName);
            }
        } catch (IOException e) {
            Log.e(context.getClass().getSimpleName(), "Error getting asset as string: " + e.getMessage());

        }
        return assetString;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connec =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connec.getActiveNetworkInfo() == null) {
            return false;
        } else if (connec.getActiveNetworkInfo().getState() == android.net.NetworkInfo.State.CONNECTED ||
                connec.getActiveNetworkInfo().getState() == android.net.NetworkInfo.State.CONNECTING ) {
            return true;
        } else if (
                connec.getActiveNetworkInfo().getState() == android.net.NetworkInfo.State.DISCONNECTED ||
                        connec.getActiveNetworkInfo().getState() == android.net.NetworkInfo.State.DISCONNECTED  ) {
            return false;
        }

        return false;
    }

}
