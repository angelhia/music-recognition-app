package avdf.com.musicrecognitionapp.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ObservableArrayList;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.gracenote.gnsdk.GnAssetFetch;
import com.gracenote.gnsdk.GnException;
import com.gracenote.gnsdk.GnUser;

import avdf.com.musicrecognitionapp.MusicResult;
import avdf.com.musicrecognitionapp.R;
import avdf.com.musicrecognitionapp.databinding.ItemResultBinding;

/**
 * Created by Angelhia on 31/7/2017.
 */

public class ResultAdapter extends BaseAdapter {

    public ObservableArrayList<MusicResult> list;
    LayoutInflater inflater;
    GnUser user;
    AppCompatActivity activity;

    public ResultAdapter(ObservableArrayList<MusicResult> list, GnUser user, AppCompatActivity activity) {
        this.list = list;
        this.user = user;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        if (inflater == null) {
            inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        ItemResultBinding binding = DataBindingUtil.inflate(inflater, R.layout.item_result, viewGroup, false);
        binding.setResult(list.get(i));

        loadAndDisplayCoverArt(list.get(i).getImageUrl(), binding.coverArtImage);


        return binding.getRoot();
    }

    /**
     * Helpers to load and set cover art image in the application display
     */
    void loadAndDisplayCoverArt(String coverArtUrl, ImageView imageView) {
        Thread runThread = new Thread(new CoverArtLoaderRunnable(coverArtUrl, imageView));
        runThread.start();
    }

    class CoverArtLoaderRunnable implements Runnable {

        String coverArtUrl;
        ImageView imageView;

        CoverArtLoaderRunnable(String coverArtUrl, ImageView imageView) {
            this.coverArtUrl = coverArtUrl;
            this.imageView = imageView;
        }

        @Override
        public void run() {

            Drawable coverArt = null;

            if (coverArtUrl != null && !coverArtUrl.isEmpty()) {
                try {
                    GnAssetFetch assetData = new GnAssetFetch(user, coverArtUrl);
                    byte[] data = assetData.data();
                    coverArt = new BitmapDrawable(BitmapFactory.decodeByteArray(data, 0, data.length));
                } catch (GnException e) {
                    e.printStackTrace();
                }

            }

            if (coverArt != null) {
                setCoverArt(coverArt, imageView);
            } else {
                setCoverArt(activity.getResources().getDrawable(R.drawable.no_image), imageView);
            }

        }

    }

    private void setCoverArt(Drawable coverArt, ImageView coverArtImage) {
        activity.runOnUiThread(new SetCoverArtRunnable(coverArt, coverArtImage));
    }

    class SetCoverArtRunnable implements Runnable {

        Drawable coverArt;
        ImageView coverArtImage;

        SetCoverArtRunnable(Drawable locCoverArt, ImageView locCoverArtImage) {
            coverArt = locCoverArt;
            coverArtImage = locCoverArtImage;
        }

        @Override
        public void run() {
            coverArtImage.setImageDrawable(coverArt);
        }
    }

}
